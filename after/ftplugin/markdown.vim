setlocal synmaxcol=1000

" For pear_tree plugin, new pair '_':
let b:pear_tree_pairs={
      \   '(': {'closer': ')'},
      \   '[': {'closer': ']'},
      \   '{': {'closer': '}'},
      \   "'": {'closer': "'"},
      \   '"': {'closer': '"'},
      \   '_': {'closer': '_'},
      \   '*': {'closer': '*'}
      \ }
