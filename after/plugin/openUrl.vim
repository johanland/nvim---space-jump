"
" openUrl.vim
"
" available for windows, mac, unix/linux
"
" 1. Open the url under the cursor: <leader>u
"

if &cp || exists('g:open_url')
	finish
endif
let g:open_url = 1
let s:save_cpo = &cpo
set cpo&vim

function! s:OpenUrl(url)
	if strlen(a:url)
		let urlStr = shellescape(a:url)
		let cmdStr = ''

		if has('win32') || has('win64')
			let cmdStr = 'cmd /c start "" ' . urlStr
		elseif has('mac')
			let cmdStr = 'open ' . urlStr
		elseif has('unix')
			" unix/linux
			let cmdStr = 'xdg-open ' . urlStr
		else
			echomsg 'Url "' . urlStr . '" can NOT be opened!'
			return
		endif

		call system(cmdStr)
		echomsg cmdStr

	endif
endfunction

"@param {String} str
"@param {String} start
"@return {Boolean}
function! s:InsensiveStartWith(str, start)
	return stridx(tolower(a:str), tolower(a:start)) == 0
endfunction

function! s:ParseUrl(testString)
	let text = a:testString
	" uri specification
	" @see http://tools.ietf.org/html/rfc3986#section-3.1
	"
	"        foo://example.com:8042/over/there?name=ferret#nose
	"        \_/   \______________/\_________/ \_________/ \__/
	"         |           |            |            |        |
	"      scheme     authority       path        query   fragment
	"         |   _____________________|__
	"        / \ /                        \
	"        urn:example:animal:ferret:nose
	"
	"
	"
	"@see http://blog.mattheworiordan.com/post/13174566389/url-regular-expression-for-links-with-or-without-the

	" mailto:
	let url = matchstr(text, '\(mailto:\)\?[A-Za-z0-9\.\-;:&=+\$,\w~%\/\!?#_]\+@[A-Za-z0-9\.\-;:&=+\$,\w~%\/\!?#_]\+')

	if s:InsensiveStartWith(url, 'mailto:')
		"do nothing
	elseif strlen(url)
		let url = 'mailto:' . url
	else
		let url = ''
	endif

	if !strlen(url)

		" http://
		" file:///
		let url = matchstr(text, '[A-Sa-s]\{3,9\}:\(\/\{2,3\}\)\?[A-Za-z0-9\.\-;:&=+\$,\w~%\/\!?#_]\+')

		if !strlen(url)
			"[example].domain
			"[sub.example].domain
			" WIP:
			"[sub.example].domain/[example]
			"[sub.example].domain#[example]
			"[sub.example].domain?[example]
			let url = matchstr(text, '[A-Za-z0-9\.\-;:&=+\$,\w~%\/\!?#_]\+\.[a-z]\{2,\}[A-Za-z0-9\.\-;:&=+\$,\w~%\/\!?#_]\?')
			"                                                               \_________/
			"                                                                    |
			"                                                                top-level
		endif
	endif

	return url
endfunction

function! s:OpenUrlUnderCursor()
	" TODO: Open URL under cursor. If none, open first/only URL at current line.
	" Current: Open URL under cursor.

	" Yank till end, could perhaps use `expand('<cWORD>')` somehow
	execute 'normal viWy'

	let text = @0
	let url = s:ParseUrl(text)

	" (Uncomment for testing):
	" echo text
	" echo url
	" return

	if strlen(url)
		" TODO: rm ending ., :, /, \, ), ], }, or !
		call s:OpenUrl(text)
	else
		echomsg 'Url is not found ...'
	endif
endfunction

command -nargs=0 OpenUrl call s:OpenUrlUnderCursor()

if !exists('g:open_url_custom_keymap')
	nnoremap gx :OpenUrl<CR>
endif

let &cpo = s:save_cpo
