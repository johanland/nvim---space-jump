" vim:fdm=marker

" ## Documentation {{{1

" Key-mapping doc. form:
" [Initial mapping]              (Explanation)
" (VIM MODE).......[End mapping] (Explanation)

" ***

" <leader>
" ........<leader>*motion* (Letter mapping for jumping, by Easymotion plugin.)
" (N).....<TAB>            (Open last edited buffer.)
"
" <leader>a                (Buffer:)
" (N)......s               (Add semicolon to end of line.)
"
" <leader>b                (Buffer:)
" (N)......l               (List and choose.)
"
" <leader>c                (Comment/clear:)
" (N/V)....c               (Toggle comments for line/visual. Also gcc.)
" (N)......s               (Clear search highlight)
"
" <leader>e                (Edit command, `:edit **/*`.)
"
" <leader>f                (Format/file:)
" (V)......a               (Format align---easy align plugin.)
" (N)......b               (File tree of buffer directory.)
" (V)......n               (No format. Remove hard wrap for visual selection.)
" (N)......p               (Paragraph format. Hard wrap paragrah.)
" (N)......t               (File tree of working directory.)
" (V)......v               (Visual format. In visual mode, hard wrap selection.)
"
" <leader>g                (Get/Git:)
" (N)......c               (Color highlight group under cursor---echo.)
" (N)......g               (Open :Gcommit ready.)
" (N)......l               (Git log.)
" (N)......n               (Next Gitgutter hunk.)
" (N)......p               (Previous Gitgutter hunk.)
" (N)......s               (Git status.)
"
" <leader>i                (Insert:)
" (N)......s               (Semi colon, insert to end of line.)
"
" <leader>o                (Open:)
" (N)......d               (Duckduckgo width word under cursor)
" (X)......d               (Duckduckgo width selection)
"
" <leader>t                (Tab or toggle:)
" (N)......8               (80 character colorcolumn.)
" (N)......i               (Indent settings cycle between soft and hard tabs of two spaces.)
" (N)......n               (Number. Cycle between nonumber, relativenumber and number.)
" (N)......S               (Spell check)
" (N)......t               (`:tabnew %`. Newtab of current file. TODO: keep cursor position.)
"
" <leader>w                (Window:)
" (N)......e               (Edit command to open in vertical split. `:sedit **/*`.)
" (N)......s               ('Single' out current window, close all other windows in current tab.)
" (N)......w               (Window split vertical. `:vsplit<cr>`.)
"
" <leader>y                (Yank:)
" .........a               (All)


" ### Insert mode mappings

" <TAB>   Start/next autocomplete
" <S-TAB> Prev autocomplete
" <C-w>   Delete word backwards


" ### Some default mappings cheats

" zR   open all folds
" zM   close all folds
" za   toggle fold at cursor position
" zj   move down to start of next fold
" zk   move up to end of previous fold

" Perform a 'search and replace' in all buffers (all listed with `:ls`):
" :bufdo %s/pattern/replace/ge | update

" }}}1

" ## Init {{{1

if has('nvim')
	"nothing ...
else
	if has('vim_starting')
		if &compatible
			set nocompatible  "no legacy support
			set all&          "reset everything to their defaults
		endif
	endif

	"Should this be for nvim also?
	let g:make = 'gmake'
	if system('uname -o') =~ '^GNU/'
		let g:make = 'make'
	endif
endif

if !exists('g:colorscheme')
	" Set name of your default colorscheme
	let g:colorscheme = 'paper'
	" Set what background color it has
	let g:backgroundShade = 'light'
endif

" }}}1

" ## Base configuration {{{1

let mapleader = "\<space>"

set clipboard+=unnamedplus
set hidden       " Allow buffer switching without saving.
set iskeyword+=- " Include '-', eg, as break when jumping between words.
set linebreak
set showfulltag
set spelllang=en " Default language for spell check.
set title        " Sets file title when possible.

" ### Editing

set copyindent
set fileencoding=utf-8
set fo=croqn
set modeline
set nojoinspaces  " Use one space, not two, after punctuation.
set textwidth=80
set nostartofline            " Saves cursor column position between buffer change.

" ### Tabs (tabs are tabs)

set shiftround
set shiftwidth=3
set softtabstop=2
set tabstop=3
" Uncomment for soft tabs (2 spaces):
" set expandtab

" ### Auto completion

set path=.,,**
set wildmode=longest:full,full
set wildignorecase
set wildignore+=*DS_Store*
set wildignore+=*/.git/*,*/.hg/*,*/.svn/*
set wildignore+=*.png,*.jpg,*.gif
set wildignore+=**/node_modules/** 
set completeopt=menuone,preview,noselect
" set wildcharm=<C-Z>
set wildcharm=<TAB>

" ### Search and replace

set ignorecase " Ignore case when searching.
set gdefault " Always do global substitutions
set smartcase  " Ignore case on search unless specified.

" Prefer ag:
if executable('ag')
	set grepprg=ag\ --nogroup\ --column\ --smart-case\ --nocolor\ --follow
	set grepformat=%f:%l:%c:%m
endif

" ### Undo, backup, view/session

set viewoptions=folds,options,cursor,curdir,unix,slash " unix/windows compatibility
set sessionoptions=blank,curdir,folds,help,resize
" set backup

" (For COC) Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Turn off swap (maybe turned on for ssh...)
set noswapfile

" ### General auto commands

" Triger `autoread` when files changes on disk
" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
" https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
augroup General
	autocmd!
	autocmd FocusGained,BufEnter * call EnterFile()
	" Notification after file change
	" https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
	autocmd FileChangedShellPost *
				\ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None
augroup END

function! EnterFile()
	if mode() != 'c'
		checktime
	endif
endfunction

" }}}1

" ## Vim-Neovim diff {{{1

if has('nvim')
	set noshelltemp "use pipes
	set backupdir=~/.nvim/backup/
	set inccommand=nosplit
	set shada+=n~/.nvim/shada                              " Store the additional state of session.
	set viewdir=~/.nvim/view/

	" Swap
	set directory=./.nvim-swap//
	set directory+=~/.nvim/swap//
	set directory+=~/tmp//
	set directory+=.

	if exists("+undofile")
		set undodir=./.nvim-undo//
		set undodir+=~/.nvim/undo//
		set undofile
	endif
else
	set autoindent
	set autoread                   " auto reload if file saved externally
	set backspace=indent,eol,start " Allow backspacing everything in insert mode.
	set display+=lastline
	set encoding=utf-8
	set fillchars=vert:\│
	set hlsearch                   " Hilight search matches
	set incsearch                  " vim will search for text as you enter it.
	set showcmd
	set showmatch                  " Highlight matching brackets, etc.
	set smarttab
	set t_vb=                      " Disable sounds:
	" backup
	set backupdir-=.
	set backupdir+=.
	set backupdir-=~/
	set backupdir^=~/.vim/backup/
	set backupdir^=./.vim-backup/
	" view
	set viewdir=~/.vim/view/
	" swap
	set directory=./.vim-swap//
	set directory+=~/.vim/swap//
	set directory+=~/tmp//
	set directory+=.

	set viminfo+=n~/.vim/viminfo " Store the additional state of session.

	if exists("+undofile")
		set undodir=./.vim-undo//
		set undodir+=~/.vim/undo//
		set undofile
	endif
endif

" }}}1

" ## Base interface configuration {{{1

set breakindent             " Every wrapped line will continue visually indented.
" set cursorline
set list
set listchars=tab:-\ ,trail:_,extends:>,precedes:<
set foldmethod=indent       "fold via syntax of files
set foldlevelstart=99       "open all folds by default
set mouse=a
set number
set relativenumber
set scrolloff=1
" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
	" Recently vim can merge signcolumn and number column into one
	set signcolumn=number
else
	set signcolumn=yes
endif
set synmaxcol=200 " Improve performance for files with long lines
set updatetime=300 " Smaller updatetime for CursorHold & CursorHoldI

" ### Statusline and ruler (when no statusline)

function! WindowNumber()
	return tabpagewinnr(tabpagenr())
endfunction

" Use in statusline as `%{StatusGitInfo()}`
function! StatusGitInfo()
	if exists("*fugitive#head")
		let l:gitStatus = fugitive#head()
		return strlen(l:gitStatus) ? '(' . l:gitStatus . ')' : ''
	endif
	return ''
endfunction

set laststatus=2
set ruler
set rulerformat=%=<%P\ %M%t%<>
set statusline=\ %M%{WindowNumber()}\ ╲b%n\ ╲%f\ %{StatusGitInfo()}%=%r%w%y%<[%{&spelllang}]╱\ l:%l/%L\ c:%c

"}}}1

" ## Mappings for vanilla (n)vim {{{1

" ### Buffers
nn <leader>bb <C-^>
nn <leader><TAB> :call feedkeys(':b<space>**/*', 't')<cr>
nn <leader>bn :bn<CR>
nn <leader>bp :bp<CR>
nn <silent><C-k> :bn<CR>
nn <silent><C-j> :bp<CR>
" List and select buffer:
nn <silent> <leader>bl :ls<CR>:buffer<Space>
nn <silent> <leader>ls :ls<CR>:buffer<Space>
" For killing buffers and keeping window layout
nn <silent> <leader>bd  <C-^>:bdelete #<CR>

" ### Command line
" Open command line:
nn Q q:
" Ctrl-p/-n increments like up/down arrow:
cno <C-p> <up>
cno <C-n> <down>
cno .. ../

" ### Editing
" Indent in visual mode and reselect block
xnoremap < <gv
xnoremap > >gv
" Increment number
nn <leader>in <C-a>
" Hard formatting the current paragraph or selection. Hard wrapping is mostyle For comments, hence
xnoremap <leader>fv gwgv
nn <leader>fp gwap
" Unwrap a hard wrapped visual section
xnoremap <leader>fn ipJ
" Save file
nn <leader>fs :w<cr>
nn <leader>s :w<cr>
" Redo undo
nn <C-y> <C-r>
" Yank all
nn <leader>ya :%y+<cr>
" Visualize all
nn <leader>va ggVG
" Add semicolon
nn <leader>as A;<esc>

" Snippets:
ino con<cr> console.log(

" End of line:
noremap + $
" When selecting til end of line, don't get line break:
vnoremap + $h
" Start of line:
" For normal mode, 0 goes to first char on line:
noremap 0 ^

" Create new lines with leader-enter/-backspace or leader-i(nsert)-j/k
nn <silent> <leader><cr>  :call InsertNewLine('o')<cr>
nn <silent> <leader>ij  :call InsertNewLine('o')<cr>
" create new line above
nn <silent> <leader><bs>  :call InsertNewLine('O')<cr>
nn <silent> <leader>ik  :call InsertNewLine('o')<cr>

" Function for inserting new lines:
function! InsertNewLine(insertMotion) abort "{{{2
	let l:options = &formatoptions
	execute 'set formatoptions-=o'

	if a:insertMotion ==# 'o'
		execute 'normal! o'
		stopinsert!
	else
		execute 'normal! O'
		stopinsert!
	endif

	" restore original formatoption
	execute 'set formatoptions+=' . l:options
endfunction "}}}2

" Wrap setting commands {{{2
command! -nargs=* WrapDefault
			\ setlocal
			\ wrap
			\ formatoptions=-t
			\ linebreak
			\ list
			\ breakindent
			\ textwidth=80
command! -nargs=* WrapSoft
			\ setlocal
			\ wrap
			\ fo-=t
			\ linebreak
			\ list
			\ breakindent
			\ textwidth=0
command! -nargs=* WrapHard
			\ setlocal
			\ wrap
			\ fo+=t
			\ linebreak
			\ list
			\ breakindent
			\ textwidth=80
command! -nargs=* WrapNone
			\ setlocal
			\ nowrap
			\ fo-=t
			\ list
"}}}2

" ### Escape

" 'qq'
inoremap qq <esc>
nnoremap <leader>qq :q!<cr>
xn <cr> <esc>
xn qq <esc>
xn <leader>qq <esc>
" 'ZZ' and 'ZQ'
ino ZQ <esc>:q!<cr>
ino ZZ <esc>:wq<cr>
" 'ctrl-...'
nn <c-c><c-c> :q<cr>
nn <c-x><c-c> :q<cr>
ino <C-C><C-C> <ESC>

" ### Interface

" TODO: How can this be done local to buffer only?
nn <Leader>zz :let &scrolloff=999-&scrolloff<CR>

" Netrw settings
nn <leader>ft :e.<CR>

" Open Ranger
nn <leader>fr :tabnew term://ranger<cr>

" 'j'/'k' for moving between rows when soft wrapped:
nn <expr> j v:count ? (v:count > 5 ? "m'" . v:count : '') . 'j' : 'gj'
nn <expr> k v:count ? (v:count > 5 ? "m'" . v:count : '') . 'k' : 'gk'
xn <expr> j v:count ? (v:count > 5 ? "m'" . v:count : '') . 'j' : 'gj'
xn <expr> k v:count ? (v:count > 5 ? "m'" . v:count : '') . 'k' : 'gk'

" Moving between vim tags / follow links:
nn <leader>l <C-]>
nn <silent><C-l> <C-]>
nn <silent><C-h> <C-o>

" Refresh:
nn <silent> <leader>r :so $MYVIMRC<CR>:syntax sync fromstart<CR>

" ### Open help files

autocmd FileType help wincmd L

" ### Search Duckduckgo

nn <leader>od :call SearchDuckduckgo(expand('<cWORD>'))<cr>
xn <leader>od y:call SearchDuckduckgo('<C-r>"')<cr>gv

function! SearchDuckduckgo(searchString)
	if strlen(a:searchString)
		let cmd = ''
		let term = shellescape(a:searchString)

		" For Macos
		if has('mac')
			let cmd = 'open https://duckduckgo.com/' . term
		endif

		call system(cmd)
	endif
	echomsg a:searchString
endfunction

" ### Search

" Clear search hilights
nn <silent> <leader>cs :nohlsearch<CR><C-L>
" nn <silent> <ESC> :nohlsearch<CR><C-L> " 'Bug': Triggers replace-mode on new vim instance
nn <silent> <CR> :nohlsearch<CR><C-L>
" Substitute word under cursor and dot repeat:
nnoremap <leader>cw :let @/='\<'.expand('<cword>').'\>'<CR>cgn
xnoremap <leader>cw "sy:let @/=@s<CR>cgn

" ### Tabs

" New tab with current buffer and jump to same line
nn <leader>tt mc:tabnew %<cr>`czz

" ### Terminal

augroup TerminalConfig
	autocmd!
	autocmd TermOpen * startinsert " start term in insert
augroup END

" TODO: Look for term buffer, if not found, open new
nn <leader>ot :terminal<cr>

" Esc
:tnoremap qq <C-\><C-n>

" ### Windows

nn          <leader>wd :q!<cr>
nn <silent> <leader>wk :wincmd k<cr>
nn <silent> <leader>wK :wincmd K<cr>
nn <silent> <leader>wj :wincmd j<cr>
nn <silent> <leader>wJ :wincmd J<cr>
nn <silent> <leader>wh :wincmd h<cr>
nn <silent> <leader>wH :wincmd H<cr>
nn <silent> <leader>wl :wincmd l<cr>
nn <silent> <leader>wL :wincmd L<cr>
nn <silent> <leader>wR :wincmd R<cr>
nn <silent> <leader>wo :wincmd r<cr>
nn <silent> <leader>ws :wincmd o<cr>
nn <silent> <leader>w- :sp<cr>
nn <silent> <c-w>- :sp<cr>
nn <silent> <leader>w\| :vsp<cr>
nn <silent> <c-w>\| :vsp<cr>
nn <silent> <leader>ww :vsplit<cr>
nn <silent> <leader>w= :wincmd =<cr>
" insert placeholder window to the left
nn <leader>wp :new<cr>:setlocal nobuflisted nonumber norelativenumber<cr>:wincmd H<cr>:wincmd p<cr>
" Jump to window number
let i = 1
while i <= 9
	execute 'nn <silent> <Leader>' . i . ' :' . i . 'wincmd w<CR>'
	let i = i + 1
endwhile

" ### Edit config
nn <leader>fed :tabnew $MYVIMRC<CR>
nn <leader>fet :edit ~/.tmux.conf<CR>
nn <leader>fez :edit ~/.zshrc<CR>
nn <leader>feb :edit ~/.bashrc<CR>

" ### Toggle settings
function! s:toggle(op)
	return eval('&'.a:op) ? 'no'.a:op : a:op
endfunction

function! s:toggleMap(letter, option)
	exe 'nn <leader>t'.a:letter.' :setlocal <C-R>=<SID>toggle("'.a:option.'")<CR><CR>'
endfunction

" toggle list
call s:toggleMap('l', 'list')

" Toggle spell check
call s:toggleMap('S', 'spell')

" Toggle cursorline/column
call s:toggleMap('c', 'cursorline')
call s:toggleMap('u', 'cursorcolumn')

" Toggle colorcolumn +1 of textwidth
nn <leader>t8 :exe 'let <c-r>=
			\ &colorcolumn < 1 ?
			\   "&colorcolumn = &textwidth + 1"
			\ :
			\   "&colorcolumn = 0"<cr>
			\ '<cr>

" Toggle statusline
nn <silent> <leader>ts :call ToggleStatus()<CR>
function! ToggleStatus() abort
	if &laststatus!=2
		set laststatus=2
	else
		set laststatus=0
	endif
endfunction

" Toggle number/relativenumber/nonumber
nn <silent> <leader>tn :call CycleNumberVariants()<CR>
function! CycleNumberVariants() abort
	if !&number && !&relativenumber
		setlocal relativenumber
		setlocal number
	elseif &number && &relativenumber
		setlocal norelativenumber
	else
		setlocal nonumber
		setlocal norelativenumber
	endif
endfunction

"Toggle tabs/spaces
function! SoftTabs(i)
	exe  'setlocal  autoindent  smarttab  expandtab  tabstop=' . a:i .'  shiftwidth=' . a:i .'  softtabstop=' . a:i .'  copyindent'
endfunction
function! HardTabs(i)
	exe 'setlocal  autoindent  smarttab  noexpandtab  tabstop=' . a:i . '  shiftwidth=' . a:i . '  softtabstop=' . a:i . '  copyindent'
endfunction
nn <silent> <leader>ti :call SetIndent(2)<CR>
function! SetIndent(i) abort
	if !&expandtab
		:call SoftTabs(a:i)
		echo 'Soft tabs set to ' .a:i. ' spaces.'
	else
		:call HardTabs(a:i)
		echo 'Hard tabs set to display as ' .a:i. ' spaces.'
	endif
endfunction

"}}}1

" ## Plugin manager settings {{{1

filetype on
filetype indent plugin off

if has('nvim')
	call plug#begin('~/.local/share/nvim/plugged')
else
	call plug#begin('~/.vim/plugged')
endif

" }}}1

" ## General plugins {{{1

Plug 'tmux-plugins/vim-tmux-focus-events'

" Auto pairer
Plug 'tmsvg/pear-tree'
imap qq <Plug>(PearTreeFinishExpansion)

" Go to previous cursor position when reading buffer
Plug 'mmozuras/vim-cursor'

" ### File explorer {{{2
	let loaded_netrwPlugin = 1 " Do not load netrw

	Plug 'cocopon/vaffle.vim'
	nn <leader>fb :Vaffle %<CR>

	function! s:customize_vaffle_mappings() abort
		" Customize key mappings here
		" nmap <buffer> <Bslash> <Plug>(vaffle-open-root)
		nmap <buffer> O        <Plug>(vaffle-mkdir)
		nmap <buffer> o        <Plug>(vaffle-new-file)
		nmap <buffer> cc <Plug>(vaffle-rename-selected)
	endfunction
	augroup vimrc_vaffle
		autocmd!
		autocmd FileType vaffle call s:customize_vaffle_mappings()
	augroup END
" }}}

" ### File history "{{{2
	Plug 'mbbill/undotree', { 'on': 'UndotreeToggle' }
	let g:undotree_SetFocusWhenToggle=1
	nn <silent> <leader>fu :UndotreeToggle<CR>
"}}}2

" ### Fuzzy finder {{{2
	Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
	Plug 'junegunn/fzf.vim', { 'on': ['Files', 'Ag', 'Buffers'] }

	" An action can be a reference to a function that processes selected lines
	function! s:build_quickfix_list(lines)
		call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
		copen
		cc
	endfunction

	let g:fzf_action = {
				\ 'ctrl-q': function('s:build_quickfix_list'),
				\ 'ctrl-t': 'tab split',
				\ 'ctrl-x': 'split',
				\ 'ctrl-v': 'vsplit' }

	nn <leader><TAB> :Buffers<cr>
	nn <leader>ff :FZF<cr>
	nn <leader>fw :Ag 
	nn <leader>aa :Ag 
	nn <leader>tb :tabnew<cr>:Buffers<cr>
	" Ag for word under cursor:
	nn <leader>ag :Ag <C-r><C-w><CR>
	nn <leader>gt :Ag <C-r><C-w><CR>
	" Ag for visual selection:
	xn <leader>aa y:Ag <C-r>"
	xn <leader>ag y:Ag <C-r>"
" }}}2

" ### Git {{{2
	nn <leader>gb :!git branch<cr>
	nn <leader>gc :!git checkout 
	nn <leader>gm :!git merge 
	Plug 'tpope/vim-fugitive'
	nn <leader>gs :Gstatus<CR>
	nn <leader>gg :Git commit 
	nn <leader>gp :Git push 
	Plug 'junegunn/gv.vim', { 'on': 'GV'}
	nn <leader>gl :GV<CR>
	" Git blame
	Plug 'rhysd/git-messenger.vim', { 'on': 'GitMessenger'}
	let g:git_messenger_no_default_mappings=v:true
	Plug 'airblade/vim-gitgutter'
	nmap <leader>gj <Plug>(GitGutterNextHunk)
	nmap <leader>gk <Plug>(GitGutterPrevHunk)
" }}}

" ### Auto completion and linting {{{2
	" COC {{{3
		Plug 'neoclide/coc.nvim', {'branch': 'release'}

		" (For COC) Don't pass messages to |ins-completion-menu|.
		set shortmess+=c

		nmap <silent> <leader>ci <Plug>(coc-diagnostic-info)
		nmap <silent> <leader>cp <Plug>(coc-diagnostic-prev)
		nmap <silent> <leader>cn <Plug>(coc-diagnostic-next)

		" Use tab for trigger completion with characters ahead and navigate.
		" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
		inoremap <silent><expr> <TAB>
					\ pumvisible() ? "\<C-n>" :
					\ <SID>check_back_space() ? "\<TAB>" :
					\ coc#refresh()
		inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

		function! s:check_back_space() abort
			let col = col('.') - 1
			return !col || getline('.')[col - 1]  =~# '\s'
		endfunction

		" Use <c-space> to trigger completion.
		if has('nvim')
			inoremap <silent><expr> <c-space> coc#refresh()
		else
			inoremap <silent><expr> <c-@> coc#refresh()
		endif

		" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
		" position. Coc only does snippet and additional edit on confirm.
		" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
		if exists('*complete_info')
			inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
		else
			inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
		endif

		" GoTo code navigation.
		nmap <silent> gd <Plug>(coc-definition)
		nmap <silent> gy <Plug>(coc-type-definition)
		nmap <silent> gi <Plug>(coc-implementation)
		nmap <silent> gr <Plug>(coc-references)

		" Use K to show documentation in preview window
		nnoremap <silent> K :call <SID>show_documentation()<CR>

		function! s:show_documentation()
			if (index(['vim','help'], &filetype) >= 0)
				execute 'h '.expand('<cword>')
			else
				call CocAction('doHover')
			endif
		endfunction

		" Using CocList
		" Show all diagnostics
		nnoremap <silent> <leader>cd  :<C-u>CocList diagnostics<cr>
		" Manage extensions
		nnoremap <silent> <leader>ce  :<C-u>CocList extensions<cr>
	" }}}3

	" Ale {{{
		Plug 'dense-analysis/ale', {'for': 'javascript'}
		let g:ale_disable_lsp = 1
		let g:ale_linters = {
					\   'svelte': ['eslint'],
					\   'javascript': ['eslint'],
					\   'html': ['eslint'],
					\}
		let g:ale_linters_explicit = 1 " Only run linters named in ale_linters settings.
		let g:ale_fix_on_save = 1
	" }}}
" }}}2

" Yanker {{{2
	Plug 'svermeulen/vim-yoink', { 'on': [ '<plug>(YoinkPaste_p)', '<plug>(YoinkPaste_P)' ]}

	let g:yoinkIncludeDeleteOperations=1

	if has('nvim')
		let g:yoinkSavePersistently=1
	endif

	nmap <c-p> <plug>(YoinkPostPasteSwapBack)
	nmap <c-n> <plug>(YoinkPostPasteSwapForward)
	nmap p <plug>(YoinkPaste_p)
	nmap P <plug>(YoinkPaste_P)
" }}}2

" (Un)commenter
Plug 'tomtom/tcomment_vim', { 'on': 'TComment' }
nn <leader>cc :TComment<cr>
xn <leader>cc :TComment<cr>
" TODO: Add svelte
" tcomment#type#Define()

" Surrounder
Plug 'tpope/vim-surround'

" Repeater (repeat more with `.`)
Plug 'tpope/vim-repeat'

" Support for editorconfig files
Plug 'editorconfig/editorconfig-vim'

" Visual star
Plug 'bronson/vim-visual-star-search'

" Align text {{{2
	Plug 'junegunn/vim-easy-align', { 'on': '<Plug>(EasyAlign)' }
	xmap <leader>ga <Plug>(EasyAlign)
" }}}2

" Session storer (mainly used with tmux ressurect)
Plug 'tpope/vim-obsession'

" ### Motion

Plug 'unblevable/quick-scope'

" Match bloc's start/end
Plug 'adelarsq/vim-matchit'

" Focus writing {{{2
	Plug 'junegunn/goyo.vim', {'on': 'Goyo' }
	nn <silent> <leader>tf :Goyo<cr>
	function! s:goyo_enter()
		let g:goyo_width='110'
		let g:goyo_height='100%'
		set scrolloff=5
	endfunction
	function! s:goyo_leave()
		call SetColorscheme(g:colorscheme, g:backgroundShade)
		set scrolloff=1
	endfunction
	autocmd! User GoyoLeave nested call <SID>goyo_leave()
" }}}2
" }}}1

" ## File type settings and plugins {{{1

" Support for different lang in a single file (single page components)
" Pug 'Shougo/context_filetype.vim'
"
" if !exists('g:context_filetype#same_filetypes')
"   let g:context_filetype#filetypes = {}
" endif
"
" " Svelte lang syntax config
" let g:context_filetype#filetypes.svelte =
" \ [
" \   {'filetype' : 'javascript', 'start' : '<script>', 'end' : '</script>'},
" \   {
" \     'filetype': 'typescript',
" \     'start': '<script\%( [^>]*\)\? \%(ts\|lang="\%(ts\|typescript\)"\)\%( [^>]*\)\?>',
" \     'end': '',
" \   },
" \   {'filetype' : 'css', 'start' : '<style \?.*>', 'end' : '</style>'},
" \ ]
"
" let g:ft = ''

" ### CSS

" #### Post CSS
Plug 'stephenway/postcss.vim', { 'for': ['css', 'sass', 'scss', 'stylus', 'vue'] }

" #### SCSS
Plug 'cakebaker/scss-syntax.vim', {'for': ['scss', 'vue', 'html']}

" #### Stylus
Plug 'wavded/vim-stylus', { 'for': ['stylus', 'vue', 'html'] }

" ### Javascript
Plug 'pangloss/vim-javascript'
let g:javascript_plugin_jsdoc = 1
augroup javascript_folding
    au!
    au FileType javascript setlocal foldmethod=syntax
augroup END

" #### Svelte

" autocmd BufReadPost,BufNewFile *.svelte setlocal filetype=html
" autocmd BufReadPost,BufNewFile *.svelte setlocal filetype=svelte

" Plug 'evanleck/vim-svelte', { 'for': 'svelte' }
" let g:svelte_preprocessor_tags = [
"   \ { 'name': 'postcss', 'tag': 'style', 'as': 'scss' }
"   \ ]
" let g:svelte_preprocessors = ['postcss'] " You still need to enable these preprocessors as well.

Plug 'leafOfTree/vim-svelte-plugin', { 'for': 'svelte' }

" #### Vue.js
Plug 'posva/vim-vue', { 'for': 'vue' }
" let g:vue_disable_pre_processors=1
autocmd BufReadPost,BufNewFile *.vue setlocal filetype=vue

" ### HTML
autocmd BufReadPost,BufNewFile *.tpl.php,*.hbs setlocal filetype=html

" #### Pug
Plug 'digitaltoad/vim-pug', { 'for': [ 'pug', 'vue' ] }
autocmd BufReadPost,BufNewFile *.pug setlocal filetype=pug

" #### Markdown {{{2
	Plug 'godlygeek/tabular', { 'for': 'markdown' }
	Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
	let g:vim_markdown_conceal = 0
	let g:vim_markdown_frontmatter = 1 " Highlight YAML front matter
	let g:vim_markdown_emphasis_multiline = 0
	let g:vim_markdown_no_extensions_in_markdown = 1
	" Automatically inserting bulletpoints can lead to problems when wrapping text
	let g:vim_markdown_auto_insert_bullets = 0
	let g:vim_markdown_new_list_item_indent = 0

	" (Could be in after file, but since many comments also are written in;
	" markdown---it is not):
	" Surround with bold, italic
	nmap <leader>si viwS*
	nmap <leader>sb viwS_gvS_
	vmap <leader>b S_gvS_
	vmap <leader>i S*

	autocmd BufEnter *.md,*.mkd,*.markdown call SetMarkdown(3)

	function! SetMarkdown(indentSpace)
		" call HardTabs(a:indentSpace)
		WrapSoft
	endfunction

	" Open current buffer in Typora
	Plug 'wookayin/vim-typora', {'on': 'Typora'}
" }}}2

" ### Tex/latex
autocmd BufEnter *.tex call SetTexSettings()
function! SetTexSettings() abort
	setlocal filetype=tex
	setlocal spell
endfunction

" ### Help and vim files
au FileType help setlocal nolist
au FileType vim setl keywordprg=:help

" }}}1

" ## Interface and end plugin-manager {{{1

	" ### Colo (colorscheme)
	Plug 'cocopon/iceberg.vim'
	Plug 'https://gitlab.com/yorickpeterse/vim-paper'
	" Plug 'KKPMW/oldbook-vim'
	" Plug 'pgdouyon/vim-yin-yang'

	command! -nargs=* ColoIceberg call SetColorscheme('iceberg','dark')
	command! -nargs=* ColoPaper call SetColorscheme('paper','light')
	" command! -nargs=* ColoYin call SetColorscheme('yin','dark')
	" command! -nargs=* ColoYang call SetColorscheme('yang','light')

	" https://medium.com/@dubistkomisch/how-to-actually-get-italics-and-true-colour-to-work-in-iterm-tmux-vim-9ebe55ebc2be
	let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
	let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
	set termguicolors " Uses the gui version of coloschemes

	" ### End plugin-manager/dein
	call plug#end()

	" ### Colo continues

	function! SetColorscheme(schemeName,bg) abort
		hi clear
		execute 'colo ' . a:schemeName
		let g:colorscheme=a:schemeName
		let g:backgroundShade=a:bg

		if a:bg=~'dark'
			execute 'set bg=' . g:backgroundShade
		elseif a:bg=~'light'
			execute 'set bg=' . g:backgroundShade
		endif

		" Plugin specifique
		hi QuickScopePrimary gui=ITALIC,UNDERLINE
		hi QuickScopeSecondary gui=ITALIC

		if a:schemeName==?'paper'
			hi LineNr guifg=#555555
			hi NonText guifg=#B58900
			hi StatusLine gui=BOLD,UNDERLINE guibg=NONE guifg=#000000
			hi StatusLineNC gui=UNDERLINE guibg=NONE
			hi TabLine guibg=NONE guifg=#1e6fcc
			hi TabLineFill guibg=NONE
		endif

		hi htmlBold gui=BOLD
		hi htmlH1 gui=BOLD
		hi htmlH2 gui=BOLD
	endf

	call SetColorscheme(g:colorscheme, g:backgroundShade)

	" Load all plugins now.
	" Plugins need to be added to runtimepath before helptags can be generated.
	packloadall
	" Load all of the helptags now, after plugins have been loaded.
	" All messages and errors will be ignored.
	silent! helptags ALL
"}}}1
