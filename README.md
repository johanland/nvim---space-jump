# nvim---Space Jump

No longer maintained. New config: https://github.com/jssteinberg/config

This is a lightweight, opinionated, scandinavian-keyboardlayout Neovim config inspired by [Spacemacs](//spacemacs.org), and later [SpaceVim](//spacevim.org) (when that came along). Both use \<SPACE\> as their <leader> key, and the following typed keys is usually a letter related to that mapping's function. E.g., "b" for "buffer".

**Prerequisites** that you have Git installed and the following dir.s: `mkdir ~/.nvim/backup`, `mkdir ~/.nvim/view`, `mkdir
~/.nvim/swap`, `mkdir ~/.nvim/undo`.

[Plug](https://github.com/junegunn/vim-plug) is plugin-manager.

